export const PATH = {
  HOME: '/',
  LOGIN: '/login',
  REGISTER: '/signup',
  POSTS: '/posts',
  POST_SHOW: '/posts/:id',
  POST_NEW: '/posts/new',
  POST_EDIT: '/posts/:id/edit',
};
