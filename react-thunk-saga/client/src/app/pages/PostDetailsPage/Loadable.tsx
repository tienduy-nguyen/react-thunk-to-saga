import * as React from 'react';
import { lazyLoad } from 'src/utils/loadable';
import { LoadingIndicator } from 'src/app/components/LoadingIndicator';
import styled from 'styled-components/macro';

const LoadingWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PostDetailsPage = lazyLoad(
  () => import('./index'),
  (module) => module.PostDetailsPage,
  {
    fallback: (
      <LoadingWrapper>
        <LoadingIndicator />
      </LoadingWrapper>
    ),
  }
);
