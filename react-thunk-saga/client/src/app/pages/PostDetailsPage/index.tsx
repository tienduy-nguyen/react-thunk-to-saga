import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import { NavBar } from 'src/app/components/Navbar';
import { PageWrapper } from 'src/app/components/PageWrapper';
import { usePostsSlice } from '../PostPage/slice';
import { Post } from '../PostPage/slice/post.types';
import { PostItemDetails } from './container/PostItemDetails';
import { useSelector, useDispatch } from 'react-redux';
import { selectLoading, selectPostItem } from '../PostPage/slice/post.selector';
import { LoadingIndicator } from 'src/app/components/LoadingIndicator';

export interface PostDetailsProps {
  title: string;
  body: string;
}

interface PostUrlParams {
  id: string;
}

export function PostDetailsPage() {
  const [postId, setPostId] = useState(-1);

  const params: PostUrlParams = useParams();
  const dispatch = useDispatch();
  const { actions } = usePostsSlice();
  const postItem: Post = useSelector(selectPostItem(postId));
  const isLoading = useSelector(selectLoading) as boolean;

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    const id: number = Number(params.id);
    dispatch(actions.fetchPostsStarted());
    setPostId(id);
  });

  return (
    <>
      <Helmet>
        <title>{postItem?.title}</title>
        <meta name='description' content='Post page' />
      </Helmet>
      <NavBar />
      <PageWrapper>
        {isLoading && <LoadingIndicator small />}
        <PostItemDetails title={postItem?.title} body={postItem?.body} />
      </PageWrapper>
    </>
  );
}
