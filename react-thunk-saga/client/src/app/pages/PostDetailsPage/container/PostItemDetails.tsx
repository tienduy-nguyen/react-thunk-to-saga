import { PostDetailsProps } from '..';

export function PostItemDetails({ title, body }: PostDetailsProps) {
  return (
    <div className='container-fluid my-12  px-4 md:px-12 text-white align-middle'>
      <div className='my-1 px-1 w-full md:w-full lg:my-4 lg:px-4 lg:w-full mx-auto text-white'>
        {/* <!-- Article --> */}
        <article className='overflow-hidden rounded-lg shadow-lg'>
          <div>
            <img
              alt='Placeholder'
              className='block h-auto w-full'
              src='https://picsum.photos/600/400/?random'
            />
          </div>
          {/* Header */}
          <header className='flex items-center justify-between leading-tight px-0 md:py-4'>
            <h1 className='text-lg text-white'>
              <div className='text-white'>{title}</div>
            </h1>
            <p className=' text-sm text-white'>14/02/21</p>
          </header>
          {/* End Header */}

          <div className='text-white'>{body}</div>

          {/* Footer */}
          <footer className='flex items-center justify-between leading-none md:py-4'>
            <a
              className='flex items-center no-underline hover:underline text-white'
              href='!#'
            >
              <img
                alt='Placeholder'
                className='block rounded-full'
                src='https://picsum.photos/32/32/?random'
              />
              <p className='ml-2 text-sm text-white'>Tien Duy</p>
            </a>
            <a className='no-underline ' href='!#'>
              <span className='hidden'>Like</span>
              <i className='fa fa-heart'></i>
            </a>
          </footer>
        </article>
      </div>
    </div>
  );
}
