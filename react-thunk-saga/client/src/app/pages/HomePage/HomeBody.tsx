import React, { useEffect, useState } from 'react';
import logo from './assets/logo.svg';
import './assets/home.css';
import { Link } from 'react-router-dom';

export function HomeBody() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const timer = setTimeout(() => setCount(count + 1), 1000);
    return () => clearTimeout(timer);
  }, [count, setCount]);

  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <p>
          Page has been open for <code>{count}</code> seconds.
        </p>
        <a
          className='App-link'
          href='https://reactjs.org'
          target='_blank'
          rel='noopener noreferrer'
        >
          React project using redux & redux-saga
        </a>
        <Link to='/posts' className='App-link'>
          Click here to go Post page
        </Link>
      </header>
    </div>
  );
}
