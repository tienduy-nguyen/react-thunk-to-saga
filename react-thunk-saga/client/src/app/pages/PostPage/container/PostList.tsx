import styled from 'styled-components/macro';
import React, { useEffect } from 'react';
import { usePostsSlice } from '../slice';
import {
  selectError,
  selectLoading,
  selectPosts,
} from '../slice/post.selector';
import { useSelector, useDispatch } from 'react-redux';
import { Post, PostErrorType } from '../slice/post.types';
import { LoadingIndicator } from 'src/app/components/LoadingIndicator';
import { PostItem } from './PostItem';

export function PostList() {
  const { actions } = usePostsSlice();
  const posts = useSelector(selectPosts) as Post[];
  const isLoading = useSelector(selectLoading) as boolean;
  const error = useSelector(selectError);
  const dispatch = useDispatch();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(actions.fetchPostsStarted());
  });

  return (
    <>
      {isLoading && <LoadingIndicator small />}
      {posts.length > 0 ? (
        <List>
          <div className='container-fluid my-12 mx-auto px-4 md:px-12 text-white'>
            <div className='flex flex-wrap -mx-1 lg:-mx-4 text-white'>
              {posts.map((post) => {
                return (
                  <PostItem
                    key={post.id}
                    id={post.id}
                    title={post.title}
                    body={post.body}
                  />
                );
              })}
            </div>
          </div>
        </List>
      ) : error ? (
        <ErrorText>{repoErrorText(error)}</ErrorText>
      ) : null}
    </>
  );
}

export const repoErrorText = (error: PostErrorType) => {
  switch (error) {
    case PostErrorType.POST_NOT_FOUND:
      return 'There is no such posts 😞';
    default:
      return 'An error has occurred!';
  }
};

const ErrorText = styled.span`
  color: red;
`;

const List = styled.div``;
