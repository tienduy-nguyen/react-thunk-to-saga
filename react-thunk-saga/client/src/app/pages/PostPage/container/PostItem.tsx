import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { PATH } from 'src/types/path.constant';
import { truncateString } from 'src/utils/truncate-string';

interface Props {
  id: number;
  title: string;
  body: string;
}

export function PostItem({ id, title, body }: Props) {
  const [shortTitle, setShortTitle] = useState(title);
  const [shortBody, setShortBody] = useState(body);

  useEffect(() => {
    setShortTitle(truncateString(title, 25));
    setShortBody(truncateString(title, 50));
  }, [title, body]);
  return (
    <div className='my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3 text-white'>
      {/* <!-- Article --> */}
      <article className='overflow-hidden rounded-lg shadow-lg'>
        <Link to={`${PATH.POSTS}/${id}`}>
          <img
            alt='Placeholder'
            className='block h-auto w-full'
            src='https://picsum.photos/600/400/?random'
          />
        </Link>
        {/* Header */}
        <header className='flex items-center justify-between leading-tight px-0 md:py-4 md:px-1'>
          <h1 className='text-lg text-white'>
            <a className='no-underline hover:underline text-white' href='!#'>
              {shortTitle}
            </a>
          </h1>
          <p className=' text-sm text-white'>14/02/21</p>
        </header>
        {/* End Header */}

        <div className='text-white'>{shortBody}</div>

        {/* Footer */}
        <footer className='flex items-center justify-between leading-none md:py-4 md:px-1'>
          <a
            className='flex items-center no-underline hover:underline text-white'
            href='!#'
          >
            <img
              alt='Placeholder'
              className='block rounded-full'
              src='https://picsum.photos/32/32/?random'
            />
            <p className='ml-2 text-sm text-white'>Tien Duy</p>
          </a>
          <a className='no-underline ' href='!#'>
            <span className='hidden'>Like</span>
            <i className='fa fa-heart'></i>
          </a>
        </footer>
      </article>
    </div>
  );
}
