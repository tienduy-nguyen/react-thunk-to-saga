import React from 'react';
import { Helmet } from 'react-helmet-async';
import { NavBar } from 'src/app/components/Navbar';
import { PageWrapper } from 'src/app/components/PageWrapper';
import { PostList } from './container/PostList';

export function PostPage() {
  return (
    <>
      <Helmet>
        <title>Post page</title>
        <meta name='description' content='Post page' />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <PostList />
      </PageWrapper>
    </>
  );
}
