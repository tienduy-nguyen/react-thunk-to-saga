export interface Post {
  id: number;
  userId: number;
  title: string;
  body: string;
}

export interface ActionRedux {
  type: string;
  payload?: any;
}

export interface PostState {
  loading: boolean;
  posts: Post[];
  postItem: Post | null;
  error: PostErrorType | null;
}

export enum PostErrorType {
  RESPONSE_ERROR = 1,
  POST_NOT_FOUND = 2,
}
