import { takeLatest, put, call } from 'redux-saga/effects';
import { Post, PostErrorType } from './post.types';
import { request } from 'src/utils/request';
import { postsActions as actions } from './index';

function* fetchPosts() {
  try {
    const requestUrl = 'https://jsonplaceholder.typicode.com/posts';
    const posts: Post[] = yield call(request, requestUrl);
    yield put(actions.fetchPostsSuccess(posts));
  } catch (error) {
    if (error.response?.status === 404) {
      yield put(actions.fetchPostsError(PostErrorType.POST_NOT_FOUND));
    } else {
      yield put(actions.fetchPostsError(PostErrorType.RESPONSE_ERROR));
    }
  }
}

/* Saga functions */
export function* postsFromSaga() {
  yield takeLatest(actions.fetchPostsStarted, fetchPosts);
}

// function* fetchPostItem({
//   payload,
// }: ReturnType<typeof actions.fetchPostsStarted>) {
//   const id = payload;
//   try {
//     const requestUrl = `https://jsonplaceholder.typicode.com/posts/${id}`;
//     const postItem: Post = yield call(request, requestUrl);
//     yield put(actions.fetchPostItemSuccess(postItem));
//   } catch (error) {
//     if (error.response?.status === 404) {
//       yield put(actions.fetchPostsError(PostErrorType.POST_NOT_FOUND));
//     } else {
//       yield put(actions.fetchPostsError(PostErrorType.RESPONSE_ERROR));
//     }
//   }
// }

/* Action for thunk*/
// export const fetchPostsStarted = () => {
//   return {
//     type: types.FETCH_POSTS_START,
//   };
// };

// export const fetchPostsSuccess = (posts: any) => {
//   return {
//     type: types.FETCH_POSTS_SUCCESS,
//     payload: posts,
//   };
// };

// export const fetchPostsError = (errorMessage: any) => {
//   return {
//     type: types.FETCH_POSTS_ERROR,
//     payload: errorMessage,
//   };
// };
