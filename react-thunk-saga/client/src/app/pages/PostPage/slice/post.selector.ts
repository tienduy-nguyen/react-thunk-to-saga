import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'src/types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.posts || initialState;

export const selectLoading = createSelector(
  [selectDomain],
  (postsState) => postsState.loading
);

export const selectError = createSelector(
  [selectDomain],
  (postsState) => postsState.error
);

export const selectPosts = createSelector(
  [selectDomain],
  (postsState) => postsState.posts
);

export const selectPostItem = (id: number) => {
  return createSelector(
    [selectDomain],
    (postsState) => postsState.posts.filter((post) => post.id === id)[0]
  );
};
