import * as types from './post.constants';

export const fetchPostsStarted = () => {
  return {
    type: types.FETCH_POSTS_START,
  };
};

export const fetchPostsSuccess = (posts: any) => {
  return {
    type: types.FETCH_POSTS_SUCCESS,
    payload: posts,
  };
};

export const fetchPostsError = (errorMessage: any) => {
  return {
    type: types.FETCH_POSTS_ERROR,
    payload: errorMessage,
  };
};

export const fetchPosts = () => async (dispatch: any) => {
  dispatch(fetchPostsStarted);

  try {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts');
    const posts = await res.json();
    dispatch(fetchPostsSuccess(posts));
  } catch (error) {
    dispatch(fetchPostsError);
  }
};
