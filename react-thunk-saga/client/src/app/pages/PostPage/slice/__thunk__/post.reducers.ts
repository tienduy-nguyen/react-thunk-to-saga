import produce from 'immer';
import * as types from './post.constants';
import { ActionRedux, Post, PostState } from '../post.types';

const initialState: PostState = {
  loading: false,
  posts: [] as Post[],
  postItem: null,
  error: null,
};

export const postsReducers = (state = initialState, action: ActionRedux) =>
  produce(state, (draft) => {
    switch (action.type) {
      case types.FETCH_POSTS_START:
        draft.loading = true;
        break;
      case types.FETCH_POSTS_SUCCESS:
        draft.loading = false;
        draft.posts = action.payload;
        break;
      case types.FETCH_POSTS_ERROR:
        draft.loading = false;
        draft.error = action.payload;
        break;
      default:
        return state;
    }
  });
