import { createSlice } from 'src/utils/@reduxjs/toolkit';
import { Post, PostErrorType, PostState } from './post.types';
import { PayloadAction } from '@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'src/utils/redux-injectors';
import { postsFromSaga } from './post.saga';

export const initialState: PostState = {
  loading: false,
  posts: [] as Post[],
  postItem: null,
  error: null,
};

const slice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    fetchPostsStarted(state) {
      state.loading = true;
    },
    fetchPostsSuccess(state, action: PayloadAction<Post[]>) {
      state.loading = false;
      state.posts = action.payload;
    },
    fetchPostItemSuccess(state, action: PayloadAction<Post>) {
      state.loading = false;
      state.postItem = action.payload;
    },
    fetchPostsError(state, action: PayloadAction<PostErrorType>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const { actions: postsActions, reducer } = slice;

export const usePostsSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: postsFromSaga });
  return { actions: slice.actions };
};

/* Action for thunks */
// export const fetchPostsStarted = () => {
//   return {
//     type: types.FETCH_POSTS_START,
//   };
// };

// export const fetchPostsSuccess = (posts: any) => {
//   return {
//     type: types.FETCH_POSTS_SUCCESS,
//     payload: posts,
//   };
// };

// export const fetchPostsError = (errorMessage: any) => {
//   return {
//     type: types.FETCH_POSTS_ERROR,
//     payload: errorMessage,
//   };
// };
