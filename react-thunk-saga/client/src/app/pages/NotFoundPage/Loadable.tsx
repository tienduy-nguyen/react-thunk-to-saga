import * as React from 'react';
import { lazyLoad } from 'src/utils/loadable';
import { LoadingIndicator } from 'src/app/components/LoadingIndicator';

export const NotFoundPage = lazyLoad(
  () => import('./index'),
  (module) => module.NotFoundPage,
  {
    fallback: <LoadingIndicator />,
  }
);
