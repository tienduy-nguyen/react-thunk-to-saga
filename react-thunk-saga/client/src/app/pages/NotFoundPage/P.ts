import styled from 'styled-components/macro';

export const P = styled.p`
  font-size: 1rem;
  line-height: 1.5;
  /* color: ${(p) => p.theme.textSecondary}; */
  /* color: rgba(58, 52, 51, 0.7); */
  color: rgba(241, 233, 231, 0.6);
  margin: 0.625rem 0 1.5rem 0;
`;
