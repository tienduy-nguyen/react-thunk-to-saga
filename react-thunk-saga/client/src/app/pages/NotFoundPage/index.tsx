import * as React from 'react';
import styled from 'styled-components/macro';
import { P } from './P';
import { Link } from 'src/app/components/Link';
import { NavBar } from 'src/app/components/Navbar';
import { Helmet } from 'react-helmet-async';
import { StyleConstants } from 'src/styles/styles.constants';

export function NotFoundPage() {
  return (
    <>
      <Helmet>
        <title>404 Page Not Found</title>
        <meta name='description' content='Page not found' />
      </Helmet>
      <NavBar />
      <Wrapper>
        <Title>
          4
          <span role='img' aria-label='Crying Face'>
            😢
          </span>
          4
        </Title>
        <P>Page not found.</P>
        <Link to={process.env.PUBLIC_URL + '/'}>Return to Home Page</Link>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.div`
  height: calc(100vh - ${StyleConstants.NAV_BAR_HEIGHT});
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

const Title = styled.div`
  margin-top: -8vh;
  font-weight: bold;
  /* color: ${(p) => p.theme.text}; */
  color: rgba(58, 52, 51, 1);
  /* color: rgba(241, 233, 231, 1); */
  font-size: 3.375rem;
  span {
    font-size: 3.125rem;
  }
`;
