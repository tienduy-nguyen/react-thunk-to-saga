import { BrowserRouter, Route, Switch } from 'react-router-dom';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { HomePage } from './pages/HomePage/Loadable';
import { PostPage } from './pages/PostPage/Loadable';
import { GlobalStyle } from 'src/styles/global-styles';
import { PATH } from 'src/types/path.constant';
import { PostDetailsPage } from './pages/PostDetailsPage/Loadable';

export function App() {
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate='%s - React Boilerplate'
        defaultTitle='React Boilerplate'
      >
        <meta name='description' content='A React Boilerplate application' />
      </Helmet>
      <Switch>
        <Route exact path={PATH.HOME} component={HomePage} />
        <Route exact path={PATH.POSTS} component={PostPage} />
        <Route exact path={PATH.POST_SHOW} component={PostDetailsPage} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
