import styled from 'styled-components/macro';
import { Link as RouterLink } from 'react-router-dom';

export const Link = styled(RouterLink)`
  /* color: ${(p) => p.theme.primary}; */
  /* color: rgba(215, 113, 88, 1); */
  color: rgba(220, 120, 95, 1);
  text-decoration: none;
  &:hover {
    text-decoration: underline;
    opacity: 0.8;
  }
  &:active {
    opacity: 0.4;
  }
`;
