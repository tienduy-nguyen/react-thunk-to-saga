import * as React from 'react';
import { Link } from 'react-router-dom';
import { PATH } from 'src/types/path.constant';
import styled from 'styled-components/macro';

export function Logo() {
  return (
    <Wrapper>
      <Title>
        <Link to={PATH.HOME}>React Boilerplate</Link>
      </Title>
      <Description>Create React App Template</Description>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

const Title = styled.div`
  font-size: 1.25rem;
  /* color: ${(p) => p.theme.text}; */
  /* color: rgba(58, 52, 51, 1); */
  color: rgba(241, 233, 231, 1);
  font-weight: bold;
  margin-right: 1rem;
`;

const Description = styled.div`
  font-size: 0.875rem;
  /* color: ${(p) => p.theme.textSecondary}; */
  /* color: rgba(58, 52, 51, 0.7); */
  color: rgba(241, 233, 231, 0.6);
  font-weight: normal;
`;
