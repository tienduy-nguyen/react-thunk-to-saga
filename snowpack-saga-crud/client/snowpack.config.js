/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: { url: '/', static: true },
    src: { url: '/dist' },
  },
  plugins: [
    '@snowpack/plugin-react-refresh',
    '@snowpack/plugin-dotenv',
    '@snowpack/plugin-typescript',
    '@snowpack/plugin-postcss',
    ['@snowpack/plugin-babel'],
    [
      '@snowpack/plugin-webpack',
      {
        htmlMinifierOptions: false, // disabled entirely,
      },
    ],
  ],
  routes: [
    /* Enable an SPA Fallback in development: */
    { match: 'routes', src: '.*', dest: '/index.html' },
  ],
  optimize: {
    /* Example: Bundle your final build: */
    // "bundle": true,
  },
  packageOptions: {
    source: 'local',
  },
  devOptions: {
    /* ... */
    port: 3030,
  },
  buildOptions: {
    baseUrl: './',
  },
  alias: {
    src: './src',
  },
};
