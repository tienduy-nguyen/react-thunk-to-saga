import React from 'react';
import ReactDOM from 'react-dom';
import { configureAppStore } from 'src/store/configureStore';
import { Provider } from 'react-redux';
import { App } from './app';
import { HelmetProvider } from 'react-helmet-async';
import './index.css';

const MOUNT_NODE = document.getElementById('root') as HTMLElement;
const store = configureAppStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <HelmetProvider>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </HelmetProvider>
    </Provider>
  </React.StrictMode>,
  MOUNT_NODE,
);

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://snowpack.dev/concepts/hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept();
}
