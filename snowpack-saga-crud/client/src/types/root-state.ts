import { PostState } from 'src/app/pages/PostPage/slice/post.types';

export interface RootState {
  posts?: PostState;
}
