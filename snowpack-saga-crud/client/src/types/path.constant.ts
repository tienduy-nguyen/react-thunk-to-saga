export const PATH = {
  HOME: '/',
  ADMIN: '/admin',
  POSTS: '/posts',
  POST_NEW: 'posts/new',
  POST_EDIT: 'posts/:id/edit',
  POST_SHOW: 'posts/:id',
  NOTFOUND: '/',
};
