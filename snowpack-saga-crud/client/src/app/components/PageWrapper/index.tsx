import styled from 'styled-components';

export const PageWrapper = styled.div`
  width: 1200px;
  margin: 0 auto;
  padding: 0 2rem;
  box-sizing: content-box;
`;
