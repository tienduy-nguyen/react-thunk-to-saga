import { BrowserRouter, Route, Switch } from 'react-router-dom';
import React from 'react';
import { Helmet } from 'react-helmet-async';
import { NotFoundPage } from './pages/NotFoundPage/Loadable';
import { HomePage } from './pages/HomePage/Loadable';
import { GlobalStyle } from 'src/styles/global-styles';
import { PATH } from 'src/types/path.constant';
import { PostsPage } from './pages/PostPage/Loadable';

export function App() {
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - React Boilerplate"
        defaultTitle="React Boilerplate"
      >
        <meta name="description" content="A React Boilerplate application" />
      </Helmet>
      <Switch>
        <Route exact path={PATH.HOME} component={HomePage} />
        <Route exact path={PATH.POSTS} component={PostsPage} />
        {/* <Route exact path={PATH.POST_NEW} component={PostNewPage} /> */}
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
