import React from 'react';
import { useForm } from 'react-hook-form';

export function PostForm() {
  const { register, handleSubmit, errors } = useForm(); // initialize the hook
  const onSubmit = data => {
    console.log(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="form">
      <div className="field field-text">
        <label htmlFor="title">
          Title:
          <input
            type="text"
            name="title"
            ref={register({ required: true, maxLength: 200 })}
          />
          {errors.title && 'Title is required.'}
        </label>
      </div>
      <div className="field field-text">
        <label htmlFor="body">
          Body:
          <textarea
            name="body"
            ref={register({ required: true, minLength: 3 })}
          />
        </label>
      </div>

      <button type="submit" className="btn">
        Save
      </button>
    </form>
  );
}
