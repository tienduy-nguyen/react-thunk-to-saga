import React from 'react';
import { Link } from 'react-router-dom';
import { PATH } from 'src/types';

type Props = {
  loading: boolean;
};

export function PostHeading({ loading }: Props) {
  if (loading) return <p>Loading...</p>;
  return (
    <div className="mb-5">
      <div className="posts-heading">
        <h2 className="posts-heading__title">Posts</h2>
        <Link
          to={PATH.POST_NEW}
          className="border rounded bg-green-500 p-1 mr-2"
        >
          New Post
        </Link>
      </div>
    </div>
  );
}
