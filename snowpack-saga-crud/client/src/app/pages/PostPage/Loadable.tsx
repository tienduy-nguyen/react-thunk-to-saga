import React from 'react';
import { LoadingIndicator } from 'src/app/components/LoadingIndicator';
import { lazyLoad } from 'src/utils/loadable';
import styled from 'styled-components';

const LoadingWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const PostsPage = lazyLoad(
  () => import('./index'),
  module => module.PostsPage,
  {
    fallback: (
      <LoadingWrapper>
        <LoadingIndicator />
      </LoadingWrapper>
    ),
  },
);

export const PostNewPage = lazyLoad(
  () => import('./pages/NewPostPage'),
  module => module.PostNewPage,
  {
    fallback: (
      <LoadingWrapper>
        <LoadingIndicator />
      </LoadingWrapper>
    ),
  },
);
