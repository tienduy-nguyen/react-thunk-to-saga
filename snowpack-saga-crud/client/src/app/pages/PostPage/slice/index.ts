import { PayloadAction } from '@reduxjs/toolkit';
import { useInjectSaga, useInjectReducer } from 'src/utils/redux-injectors';
import { Post, PostState } from './post.types';
import { createSlice } from 'src/utils/@reduxjs/toolkit';
import postsFromSaga from './post.saga';

export const initialState: PostState = {
  loading: false,
  posts: [] as Post[],
  postItem: null,
  postItemData: null,
  currentPostItemId: -1,
  error: undefined,
};

const slice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    fetchPostsRequest(state) {
      state.loading = true;
      state.currentPostItemId = -1;
    },
    fetchPostItemRequest(state, action: PayloadAction<number>) {
      state.loading = true;
      state.currentPostItemId = action.payload;
    },
    createPostRequest(state, action: PayloadAction<Partial<Post>>) {
      state.loading = true;
      state.postItemData = action.payload;
    },
    updatePostRequest(state, action: PayloadAction<Partial<Post>>) {
      state.loading = true;
      state.postItemData = action.payload;
    },
    deletePostRequest(state, action: PayloadAction<number>) {
      state.loading = true;
      state.currentPostItemId = action.payload;
    },
    getPostsSuccess(state, action: PayloadAction<Post[]>) {
      state.loading = false;
      state.posts = action.payload;
    },
    getPostItemSuccess(state, action: PayloadAction<Post>) {
      state.loading = false;
      state.postItem = action.payload;
    },
    createPostSuccess(state, action: PayloadAction<Post>) {
      state.loading = false;
      const newPost = action.payload;
      state.posts = [newPost].concat(state.posts);
    },
    updatePostSuccess(state, action: PayloadAction<Post>) {
      state.loading = false;
      const updatedPost = action.payload;
      state.posts = state.posts.map(post =>
        post.id !== updatedPost.id ? post : updatedPost,
      );
    },
    deletePostSuccess(state, action: PayloadAction<number>) {
      state.loading = false;
      const id = action.payload;
      state.posts = state.posts.filter(post => post.id !== id);
    },
    postsError(state, action: PayloadAction<string>) {
      state.error = action.payload;
      state.currentPostItemId = -1;
    },
  },
});

export const { actions: postsActions, reducer } = slice;

export const usePostsSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: postsFromSaga });
  return { actions: slice.actions };
};
