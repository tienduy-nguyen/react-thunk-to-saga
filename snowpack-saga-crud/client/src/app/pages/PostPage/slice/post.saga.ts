import { AnyAction } from '@reduxjs/toolkit';
import { all, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { request } from 'src/utils/request';
import { postsActions as actions } from './index';
import { Post } from './post.types';

// yield functions
function* getPosts() {
  try {
    const requestUrl = `http://localhost:5000/api/posts`;
    const posts: Post[] = yield call(request, requestUrl);
    yield put(actions.getPostsSuccess(posts));
  } catch (error) {
    yield put(actions.postsError(error.message));
  }
}
function* getPostItem({ payload: id }: AnyAction) {
  try {
    const requestUrl = `http://localhost:5000/api/posts/${id}`;
    const post: Post = yield call(request, requestUrl);
    yield put(actions.getPostItemSuccess(post));
  } catch (error) {
    yield put(actions.postsError(error.message));
  }
}

function* createPost({ payload: data }: AnyAction) {
  try {
    const requestUrl = `http://localhost:5000/api/posts`;
    const newPost: Post = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    yield put(actions.createPostSuccess(newPost));
  } catch (error) {
    yield put(actions.postsError(error.message));
  }
}

function* updatePost({ payload: post }: AnyAction) {
  try {
    const requestUrl = `http://localhost:5000/api/posts/${post.id}`;
    const updated: Post = yield call(request, requestUrl, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(post),
    });
    yield put(actions.updatePostSuccess(updated));
  } catch (error) {
    yield put(actions.postsError(error.message));
  }
}

function* deletePost({ payload: id }: AnyAction) {
  try {
    const requestUrl = `http://localhost:5000/api/posts/${id}`;
    yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    yield put(actions.deletePostSuccess(id));
  } catch (error) {
    yield put(actions.postsError(error.message));
  }
}

// watcher
function* actionWatcher() {
  yield takeLatest(actions.fetchPostsRequest, getPosts);
  yield takeLatest(actions.fetchPostItemRequest, getPostItem);
  yield takeLatest(actions.createPostRequest, createPost);
  yield takeEvery(actions.updatePostRequest, updatePost);
  yield takeEvery(actions.deletePostRequest, deletePost);
}

// combine sagas
export default function* postsFromSaga() {
  yield all([actionWatcher()]);
}
