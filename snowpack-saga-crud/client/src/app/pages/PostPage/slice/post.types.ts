export interface Post {
  id: number;
  title: string;
  body: string;
  userId?: number;
}

export interface PostState {
  loading: boolean;
  posts: Post[];
  postItem: Post | null;
  postItemData: Partial<Post> | null;
  currentPostItemId: number;
  error: string | undefined;
}
