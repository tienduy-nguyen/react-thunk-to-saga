import React from 'react';
import { Link } from 'react-router-dom';
import { PostHeading } from '../components/PostHeading';
import { Post } from '../slice/post.types';

type Props = {
  loading: boolean;
  posts: Post[];
  url?: string;
  onEditPost?: (id: number) => void;
  onDeletePost?: (id: number) => void;
};

export function PostList({
  loading,
  posts,
  url,
  onEditPost,
  onDeletePost,
}: Props) {
  if (loading) return <p>Loading...</p>;
  if (posts.length === 0) return <div>No posts.</div>;
  return (
    <>
      <PostHeading loading={loading} />

      <ul className="posts">
        {posts.map(post => (
          <li
            className="posts__item list-disc ml-5 text-blue-500 mb-2"
            key={post.id}
          >
            <Link className="posts__title mr-5" to={`${url}/${post.id}`}>
              {post.title}
            </Link>
            <button
              className="mr-1 text-gray-900"
              // onClick={() => onEditPost(post.id)}
              title="Edit"
            >
              <i className="fas fa-pencil-alt"></i>
            </button>
            <button
              className=""
              // onClick={() => onDeletePost(post.id)}
              title="Delete"
            >
              <i className="fas fa-trash text-red-500"></i>
            </button>
          </li>
        ))}
      </ul>
    </>
  );
}
