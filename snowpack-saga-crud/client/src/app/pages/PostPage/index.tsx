import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useDispatch, useSelector } from 'react-redux';
import { PageWrapper } from 'src/app/components/PageWrapper';
import { PostList } from './container/PostList';
import { usePostsSlice } from './slice';
import { selectLoading, selectPosts } from './slice/post.selectors';
import { Post } from './slice/post.types';

export function PostsPage() {
  const loading = useSelector(selectLoading);
  const posts = useSelector(selectPosts) as Post[];
  const dispatch = useDispatch();
  const { actions } = usePostsSlice();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    useEffect(effect, [effect]);
  };

  useEffectOnMount(() => {
    dispatch(actions.fetchPostsRequest());
  });

  return (
    <>
      <Helmet>
        <title>Post page</title>
        <meta name="description" content="Post page" />
      </Helmet>
      <PageWrapper>
        <div>
          <PostList loading={loading} posts={posts} />
        </div>
      </PageWrapper>
    </>
  );
}
