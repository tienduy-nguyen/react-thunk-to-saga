import React from 'react';
import { PostForm } from '../components/PostForm';

export function PostNewPage() {
  return (
    <>
      <h1>New post</h1>
      <PostForm></PostForm>
    </>
  );
}
