import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { NavBar } from 'src/app/components/Navbar';
import { PageWrapper } from 'src/app/components/PageWrapper';
import { HomeBody } from './HomeBody';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React Boilerplate application homepage"
        />
      </Helmet>
      <NavBar />
      <PageWrapper>
        <HomeBody />
      </PageWrapper>
    </>
  );
}
