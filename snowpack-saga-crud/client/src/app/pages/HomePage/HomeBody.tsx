import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './assets/home.scss';

export function HomeBody() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const timer = setTimeout(() => setCount(count + 1), 1000);
    return () => clearTimeout(timer);
  }, [count, setCount]);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <p>
          Page has been open for <code>{count}</code> seconds.
        </p>

        <Link to="/posts" className="App-link mt-4 text-blue-500">
          Click here to go Post page
        </Link>
      </header>
    </div>
  );
}
